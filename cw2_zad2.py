# Zadanie 2

# Odczytujemy wiek studenta
pierwszy_stu = int(input('Podaj wiek pierwszego studenta: '))

#Odczytujemy z pliku wiek drugiego studenta
with open('wiek_drugiego_studenta.txt', 'r') as plik:
    drugi_stu = int(plik.read())

#Sprawdzamy czy czy student pierwszy jest młodszy od studenta drugiego, 
#wyswietlamy na ekranie wynik i zapisujemy do pliku

if (pierwszy_stu<drugi_stu):
    roznica_wieku = drugi_stu-pierwszy_stu
    zdanie = 'Pierwszy student jest mlodszy od studenta drugiego o '+ str(roznica_wieku) +' lat(a).'
    with open('wiek2.txt', 'w') as plik:
        plik.write(zdanie)
    print(zdanie)
